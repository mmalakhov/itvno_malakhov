function [ z ] = fz( x,y )

global H a b
z=H-a*x.^2-b*y.^2;
end