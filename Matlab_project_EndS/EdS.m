%%
clear % ������ ���������� ����������
close % ������� ��� �������� �������
n=299; % ����� ���������
global H a b % ���������� ���������� ����� � ���� �������
a=1;
b=1;
H=6;
Q=1;
L=3; % ������ �������� ��������������


h=2*L/(n-1);  %  ���
% ������ �����:
[x,y]=meshgrid(-L:h:L,-L:h:L); 

z=H-a*x.^2-b*y.^2;
% ����� � ���� ��������� �������� ������������
% ������������ ������

% ������ ������:
h1=figure(1) % ����� �������
mesh(-L:h:L,-L:h:L,z) % ������
title('Surface') % ������� �������
xlabel('x') 
ylabel('y')
zlabel('z')
% ���������
%saveas(h1,strcat('Our_Figure',  'H=', num2str(H), '.png'),'png')



dfx=zeros(n,n); % prelocation 
dfy=zeros(n,n); % ����������� ����� ��� ������
dfz=zeros(n,n); % ����� �������� ������� ����� ���������� 
ds=zeros(n,n); % � ������ ���������� � ����� ����� �������
absNablaf=zeros(n,n);
En=zeros(n,n);



r=sqrt(x.^2+y.^2+z.^2); % ������ ������

Integral=0; % �������� ��������� �� �������

for ix=1:n
    for iy=1:n
        
       zi=z(ix,iy);
       if zi>0
        % func1(x(ix,ix),y(iy,iy),zi)=0 �.�. ��� ������ ������
        dfx(ix,iy)=(func1(x(ix,iy)+h/2,y(ix,iy),zi)-func1(x(ix,iy)-h/2,y(ix,iy),zi))/h; % �����������
        dfy(ix,iy)=(func1(x(ix,iy),y(ix,iy)+h/2,zi)-func1(x(ix,iy),y(ix,iy)-h/2,zi))/h;
        dfz(ix,iy)=(func1(x(ix,iy),y(ix,iy),zi+h/2)-func1(x(ix,iy),y(ix,iy),zi-h/2))/h;
        
        z1=fz(x(ix,iy)-h/2, y(ix,iy)-h/2); % ������� ������� �������
        z2=fz(x(ix,iy)-h/2, y(ix,iy)+h/2);
        z3=fz(x(ix,iy)+h/2, y(ix,iy)-h/2);
        
        ax=0; % ���������� �������� �� �������� �������
        ay=h;
        az=z2-z1;
        bx=h;
        by=0;
        bz=z3-z1;
        
        VectMult_x=ay*bz-az*by; % ������� ����� ��������� ������������
         VectMult_y=az*bx-ax*bz;
          VectMult_z=ax*by-ay*bx;
        
          
        ds(ix,iy)=sqrt(VectMult_x.^2+VectMult_y.^2+VectMult_z.^2);
        
        %������ ��������� ��� ���������� ���������� ������� �������:
        absNablaf(ix,iy)=sqrt(dfx(ix,iy).^2+dfy(ix,iy).^2+dfz(ix,iy).^2); 
        E=Q./r(ix,iy).^3; % �������������
        
          % ���������� ������������ �������������:
       En(ix,iy)= (dfx(ix,iy).*x(ix,iy)+dfy(ix,iy).*y(ix,iy)+dfz(ix,iy).*zi).*E./absNablaf(ix,iy);    
       
       Integral=Integral+En(ix,iy).*ds(ix,iy); % ��������� ������� � �����
       end
    end
end





Integral/(2*pi)



h2=figure(2) % ����� �������
mesh(-L:h:L,-L:h:L, En) % ������
title('En') % ������� �������
xlabel('x') 
ylabel('y')
zlabel('z')
% ���������
%saveas(h1,strcat('En',  'H=', num2str(H), '.png'),'png')
